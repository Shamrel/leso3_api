/*!
\file    serialib.h
\brief   Serial library to communicate throught serial port, or any device emulating a serial port.
\author  Philippe Lucidarme (University of Angers) <serialib@googlegroups.com>
\version 1.2
\date    28 avril 2011
This Serial library is used to communicate through serial port.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This is a licence-free software, it can be used by anyone who try to build a better world.
*/


#ifndef SERIALIB_H
#define SERIALIB_H

#include <cstdio>
#include <iostream>
#include <vector>
#include <string>


using std::cout;
using std::vector;
using std::string;

// Used for TimeOut operations
#include <sys/time.h>
// Include for windows
#if defined (_WIN32) || defined( _WIN64)
    // Accessing to the serial port under Windows
    #include <windows.h>
#endif

// Include for Linux
#ifdef __linux__
    #include <stdlib.h>
    #include <sys/types.h>
    #include <sys/shm.h>
    #include <termios.h>
    #include <string.h>
    #include <iostream>
    // File control definitions
    #include <fcntl.h>
    #include <unistd.h>
    #include <sys/ioctl.h>
#endif



/*!  \class serialib
     \brief     This class can manage a serial port. The class allows basic operations (opening the connection, reading, writing data and closing the connection).
     \example   Example1.cpp
   */


class Serialib
{

private:
	#if defined (_WIN32) || defined( _WIN64)
	COMMTIMEOUTS    _timeouts;
	HANDLE
	#endif
	#ifdef __linux__
	int
	#endif
	_hSerial = 0;

public:

	enum SerialPortStatus
	{
		SERIAL_OK,
		SERIAL_DEVICE_NOT_FOUND,
		SERIAL_NOT_OPEN,
		SERIAL_ERROR_OPEN,
		SERIAL_ERROR_SPEED,
		SERIAL_ERROR_GET_PARAM,
		SERIAL_ERROR_SET_PARAM,
		SERIAL_ERROR_READ,
		SERIAL_ERROR_WRITE,
		SERIAL_ERROR_PURGE,
		SERIAL_ERROR_CLOSE
	};

	Serialib    ();
	~Serialib   ();



	Serialib(Serialib&& that);
	Serialib& operator=(Serialib&& that);

	Serialib(const Serialib&) = delete; 		//запретить копирование
	void operator=(const Serialib&) = delete; 	//запретить присваивание


	int enumSerialPorts(
			vector <string> &portName
			);

	SerialPortStatus open(
			const char *device,
			const unsigned int bauds
			);

	void close(
			);

	int write(
			const char *writeBuffer,
			const unsigned int writeBufferSize
			);

	int writeString(
			const char *string
			);

	int read(
			char *readBuffer,
			unsigned int readBufferSize,
			const unsigned int timeOut_ms=0);

	void purge(
			);

	void setRTS(
			bool set
			);
	void setDTR(
			bool set
			);

	HANDLE getHandle(
			);

};



///*!  \class     TimeOut
//     \brief     This class can manage a timer which is used as a timeout.
//   */
//// Class TimeOut
//class TimeOut
//{
//public:
//
//    // Constructor
//    TimeOut();
//
//    // Init the timer
//    void                InitTimer();
//
//    // Return the elapsed time since initialization
//    unsigned long int   ElapsedTime_ms();
//
//private:
//    struct timeval      PreviousTime;
//};
//
//
//
///*!
//  \mainpage serialib class
//
//  \brief
//       \htmlonly
//       <TABLE>
//       <TR><TD>
//            <a href="../serialibv1.2.zip" title="Download the serialib class">
//                <TABLE>
//                <TR><TD><IMG SRC="download.png" BORDER=0 WIDTH=100> </TD></TR>
//                <TR><TD><P ALIGN="center">[Download]</P> </TD></TR>
//                </TABLE>
//            </A>
//            </TD>
//            <TD>
//                <script type="text/javascript"><!--google_ad_client = "ca-pub-0665655683291467";
//                google_ad_slot = "0230365165";
//                google_ad_width = 728;
//                google_ad_height = 90;
//                //-->
//                </script>
//                <script type="text/javascript"
//                src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
//                </script>
//            </TD>
//        </TR>
//        </TABLE>
//
//        \endhtmlonly
//
//    The class serialib offers simple access to the serial port devices for windows and linux. It can be used for any serial device (Built-in serial port, USB to RS232 converter, arduino board or any hardware using or emulating a serial port)
//    \image html serialib.png
//    The class can be used under Windows and Linux.
//    The class allows basic operations like :
//    - opening and closing connection
//    - reading data (characters, array of bytes or strings)
//    - writing data (characters, array of bytes or strings)
//    - non-blocking functions (based on timeout).
//
//
//  \author   Philippe Lucidarme (University of Angers) <serialib@googlegroups.com>
//  \date     1th may 2011 (Last update: 25th september 2012)
//  \version  1.2
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
//INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
//PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM,
//DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//This is a licence-free software, it can be used by anyone who try to build a better world.
//*/




#endif // SERIALIB_H

