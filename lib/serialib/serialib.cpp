/*!
 \file    serialib.cpp
 \brief   Class to manage the serial port
 \author  Philippe Lucidarme (University of Angers) <serialib@googlegroups.com>
 \version 1.2
 \date    28 avril 2011

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE X CONSORTIUM BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


This is a licence-free software, it can be used by anyone who try to build a better world.
 */

#include "serialib.h"

#define LOGGER  (0)
#include "TraceLogger.h"


/*!
    \brief      Constructor of the class serialib.
*/
// Class constructor
Serialib::Serialib()
{
	LOG_TRACE();
    // Set TimeOut
    _timeouts.ReadIntervalTimeout = 0;
    _timeouts.ReadTotalTimeoutMultiplier = 0;
    _timeouts.ReadTotalTimeoutConstant = MAXDWORD;
    _timeouts.WriteTotalTimeoutMultiplier = 0;
    _timeouts.WriteTotalTimeoutConstant = MAXDWORD;
}

Serialib::Serialib(Serialib&& that) {
//	cout << " Serialib mov, hendle = " <<  that._hSerial << endl;
	_hSerial = that._hSerial;
	that._hSerial = 0;

#if defined (_WIN32) || defined( _WIN64)
	_timeouts = that._timeouts;
#endif
}


Serialib& Serialib::operator=(Serialib&& that) {
//		cout << " Serialib operator=, hendle = " <<  that._hSerial << endl;
	this->_hSerial = that._hSerial;
	that._hSerial = 0;
#if defined (_WIN32) || defined( _WIN64)
	this->_timeouts = that._timeouts;
#endif
	return *this;
}

/*!
    \brief      Destructor of the class serialib. It close the connection
*/
// Class desctructor
Serialib::~Serialib()
{
	LOG_TRACE();

	if((_hSerial != INVALID_HANDLE_VALUE) && (_hSerial != 0))
	{
	//	cout << __FUNCTION__ << ": " <<_hSerial << "\n";
		close();
	}
}

/**
 \return Число найденных COM
 */

int Serialib::enumSerialPorts(vector <string> &portName)
{
    TCHAR szDevices[MAXWORD];

    DWORD dwChars = ::QueryDosDevice(NULL, szDevices, MAXWORD);
    if(dwChars)
    {
        int i = 0;

        while (true)
        {
        	// Получаем текущее имя устройства
        	TCHAR* pszCurrentDevice = &szDevices[i];
        	int nLen = strlen(pszCurrentDevice);
        	if((nLen > 3) && (strncmp(pszCurrentDevice, "COM", 3)== 0))
        	{
        		portName.push_back("\\\\.\\" + string(pszCurrentDevice));
        	}

            while(szDevices[i] != '\0') i++;
            if(szDevices[++i] == '\0') break;
        }
    }

    return portName.size();
}

/*!
     \brief Open the serial port
     \param Device : Port name (COM1, COM2, ... for Windows ) or (/dev/ttyS0, /dev/ttyACM0, /dev/ttyUSB0 ... for linux)
     \param Bauds : Baud rate of the serial port.

  */

Serialib::SerialPortStatus Serialib::open(const char *device,const unsigned int bauds)
{
#if defined (_WIN32) || defined( _WIN64)

    // Open serial port
    _hSerial = CreateFileA(
    		device,GENERIC_READ | GENERIC_WRITE,
			0,
			0,
			OPEN_EXISTING,
			FILE_ATTRIBUTE_NORMAL,
			0);
    if(_hSerial==INVALID_HANDLE_VALUE) {
        if(GetLastError()==ERROR_FILE_NOT_FOUND)
        	throw SERIAL_DEVICE_NOT_FOUND;
        throw SERIAL_ERROR_OPEN;
    }

 ///   cout << "_hSerial: " << _hSerial << endl;
    // Set parameters
    DCB dcbSerialParams = {0};                                          // Structure for the port parameters
    dcbSerialParams.DCBlength=sizeof(dcbSerialParams);
    if (!GetCommState(_hSerial, &dcbSerialParams))                       // Get the port parameters
    	throw SERIAL_ERROR_GET_PARAM;

 	dcbSerialParams.BaudRate=bauds;
    dcbSerialParams.ByteSize=8;                                         // 8 bit data
    dcbSerialParams.StopBits=ONESTOPBIT;                                // One stop bit
    dcbSerialParams.Parity=NOPARITY;                                    // No parity

    if(!SetCommState(_hSerial, &dcbSerialParams))                       // Write the parameters
    	throw SERIAL_ERROR_SET_PARAM;

	if(!SetCommTimeouts(_hSerial, &_timeouts))                            // Write the parameters
		throw SERIAL_ERROR_SET_PARAM;                                  // Error while writting the parameters
	return SERIAL_OK;

#endif
#ifdef __linux__    
    struct termios options;                                             // Structure with the device's options


    // Open device
    fd = open(Device, O_RDWR | O_NOCTTY | O_NDELAY);                    // Open port
    if (fd == -1) return -2;                                            // If the device is not open, return -1
    fcntl(fd, F_SETFL, FNDELAY);                                        // Open the device in nonblocking mode

    // Set parameters
    tcgetattr(fd, &options);                                            // Get the current options of the port
    bzero(&options, sizeof(options));                                   // Clear all the options
    speed_t         Speed;
    switch (Bauds)                                                      // Set the speed (Bauds)
    {
    case 110  :     Speed=B110; break;
    case 300  :     Speed=B300; break;
    case 600  :     Speed=B600; break;
    case 1200 :     Speed=B1200; break;
    case 2400 :     Speed=B2400; break;
    case 4800 :     Speed=B4800; break;
    case 9600 :     Speed=B9600; break;
    case 19200 :    Speed=B19200; break;
    case 38400 :    Speed=B38400; break;
    case 57600 :    Speed=B57600; break;
    case 115200 :   Speed=B115200; break;
    default : return -4;
}
    cfsetispeed(&options, Speed);                                       // Set the baud rate at 115200 bauds
    cfsetospeed(&options, Speed);
    options.c_cflag |= ( CLOCAL | CREAD |  CS8);                        // Configure the device : 8 bits, no parity, no control
    options.c_iflag |= ( IGNPAR | IGNBRK );
    options.c_cc[VTIME]=0;                                              // Timer unused
    options.c_cc[VMIN]=0;                                               // At least on character before satisfy reading
    tcsetattr(fd, TCSANOW, &options);                                   // Activate the settings
    return (1);                                                         // Success
#endif
}


/*!
     \brief Close the connection with the current device
*/
void Serialib::close()
{
#if defined (_WIN32) || defined( _WIN64)
    if(!CloseHandle(_hSerial))	// If the function fails, the return value is zero.
    	throw SERIAL_ERROR_CLOSE;
    _hSerial = 0;
#endif
#ifdef __linux__
    close (fd);
#endif
}


/*!
     \brief Write an array of data on the current serial port
     \param Buffer : array of bytes to send on the port
     \param NbBytes : number of byte to send
     \return Число переданных байт
*/
int Serialib::write(
					const char *writeBuffer,
					const unsigned int writeBufferSize
					)
{
#if defined (_WIN32) || defined( _WIN64)
 // 	cout <<"_hSerial: "<< _hSerial << "\n";
	DWORD dwBytesWritten;
    if(!WriteFile(_hSerial, writeBuffer, writeBufferSize, &dwBytesWritten, NULL))
    	throw SERIAL_ERROR_WRITE;
    return (int)dwBytesWritten;
#endif
#ifdef __linux__
    if (write (fd,Buffer,NbBytes)!=(ssize_t)NbBytes)                              // Write data
        return -1;                                                      // Error while writing
    return 1;                                                           // Write operation successfull
#endif
}

/*!
     \brief Write a string on the current serial port
     \param String : string to send on the port (must be terminated by '\0')
     \return Число принятых байт
  */
int Serialib::writeString(const char *string)
{
#if defined (_WIN32) || defined( _WIN64)
    DWORD dwBytesWritten;
    if(!WriteFile(_hSerial, string, strlen(string), &dwBytesWritten,NULL))
        throw SERIAL_ERROR_WRITE;
    return (int)dwBytesWritten;
#endif
#ifdef __linux__
    int Lenght=strlen(String);                                          // Lenght of the string
    if (write(fd,String,Lenght)!=Lenght)                                // Write the string
        return -1;                                                      // error while writing
    return 1;                                                           // Write operation successfull
#endif
}

/*!
     \brief Read an array of bytes from the serial device (with timeout)
     \param Buffer : array of bytes read from the serial device
     \param MaxNbBytes : maximum allowed number of bytes read
     \param TimeOut_ms : delay of timeout before giving up the reading
     \return 1 success, return the number of bytes read
     \return 0 Timeout reached
     \return Количество принятых байт
*/
int Serialib::read(
		char *readBuffer,
		unsigned int readBufferSize,
		unsigned int timeOut_ms
		)
{
	LOG_TRACE();
#if defined (_WIN32) || defined(_WIN64)

 //   cout << "_hSerial: " << _hSerial << endl;
	DWORD bytesReceived = 0;
	_timeouts.ReadTotalTimeoutConstant=(DWORD)timeOut_ms;
//	_timeouts.ReadIntervalTimeout = 0;
//
//	cout << hex << "timeouts: " << endl;
//	cout << hex << "ReadIntervalTimeout = " << _timeouts.ReadIntervalTimeout << endl;
//	cout << hex << "ReadTotalTimeoutMultiplier = " << _timeouts.ReadTotalTimeoutMultiplier << endl;
//	cout << hex << "ReadTotalTimeoutConstant = " << _timeouts.ReadTotalTimeoutConstant << endl;
//	cout << hex << "WriteTotalTimeoutMultiplier = " << _timeouts.WriteTotalTimeoutMultiplier << endl;
//	cout << hex << "WriteTotalTimeoutConstant = " << _timeouts.WriteTotalTimeoutConstant << endl;

    if(!SetCommTimeouts(_hSerial, &_timeouts))
    	throw SERIAL_ERROR_SET_PARAM;

    if(!ReadFile(_hSerial, readBuffer, (DWORD)readBufferSize, &bytesReceived, NULL))
    	throw SERIAL_ERROR_READ;

    return (int) bytesReceived;

#endif
#ifdef __linux__
    TimeOut          Timer;                                             // Timer used for timeout
    Timer.InitTimer();                                                  // Initialise the timer
    unsigned int     NbByteRead=0;
    while (Timer.ElapsedTime_ms()<TimeOut_ms || TimeOut_ms==0)          // While Timeout is not reached
    {
        unsigned char* Ptr=(unsigned char*)Buffer+NbByteRead;           // Compute the position of the current byte
        int Ret=read(fd,(void*)Ptr,MaxNbBytes-NbByteRead);              // Try to read a byte on the device
        if (Ret==-1) return -2;                                         // Error while reading
        if (Ret>0) {                                                    // One or several byte(s) has been read on the device
            NbByteRead+=Ret;                                            // Increase the number of read bytes
            if (NbByteRead>=MaxNbBytes)                                 // Success : bytes has been read
                return 1;
        }
    }
    return 0;                                                           // Timeout reached, return 0
#endif
}

/*!
    \brief Empty receiver buffer (UNIX only)
*/

void Serialib::purge()
{
#if defined (_WIN32) || defined(_WIN64)
	DWORD flags = 0;
	flags = 	PURGE_RXABORT 	// Terminates all outstanding overlapped read operations
			|	PURGE_RXCLEAR 	// Clears the input buffer
			| 	PURGE_TXABORT	// Terminates all outstanding overlapped write operations
			|	PURGE_TXCLEAR;	// Clears the output buffer

	if(!(::PurgeComm(_hSerial, flags)))	//If the function fails, the return value is zero.
		throw SERIAL_ERROR_PURGE;

#endif
#ifdef __linux__
    tcflush(fd,TCIFLUSH);
#endif
}

void	Serialib::setRTS(bool set)
{
	if (_hSerial == 0)
		 throw SERIAL_NOT_OPEN;
	if (!::EscapeCommFunction(_hSerial, set ? SETRTS : CLRRTS)) {
		throw SERIAL_ERROR_WRITE;
	  }
}

void Serialib::setDTR(bool set)
{
	if (_hSerial == 0)
		throw SERIAL_NOT_OPEN;
	if (!::EscapeCommFunction(_hSerial, set ? SETDTR : CLRDTR)) {
		throw SERIAL_ERROR_WRITE;
    }
}

HANDLE Serialib::getHandle(
		)
{
	return _hSerial;
};

//
//// ******************************************
////  Class TimeOut
//// ******************************************
//
//
///*!
//    \brief      Constructor of the class TimeOut.
//*/
//// Constructor
//TimeOut::TimeOut()
//{}
//
///*!
//    \brief      Initialise the timer. It writes the current time of the day in the structure PreviousTime.
//*/
////Initialize the timer
//void TimeOut::InitTimer()
//{
//    gettimeofday(&PreviousTime, NULL);
//}
//
///*!
//    \brief      Returns the time elapsed since initialization.  It write the current time of the day in the structure CurrentTime.
//                Then it returns the difference between CurrentTime and PreviousTime.
//    \return     The number of microseconds elapsed since the functions InitTimer was called.
//  */
////Return the elapsed time since initialization
//unsigned long int TimeOut::ElapsedTime_ms()
//{
//    struct timeval CurrentTime;
//    int sec,usec;
//    gettimeofday(&CurrentTime, NULL);                                   // Get current time
//    sec=CurrentTime.tv_sec-PreviousTime.tv_sec;                         // Compute the number of second elapsed since last call
//    usec=CurrentTime.tv_usec-PreviousTime.tv_usec;                      // Compute
//    if (usec<0) {                                                       // If the previous usec is higher than the current one
//        usec=1000000-PreviousTime.tv_usec+CurrentTime.tv_usec;          // Recompute the microseonds
//        sec--;                                                          // Substract one second
//    }
//    return sec*1000+usec/1000;
//}
//
