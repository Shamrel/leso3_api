/*
 * TraceLogger.h
 *
 *  Created on: 18 мая 2016 г.
 *      Author: Lermash
 */

//#ifndef _TRACE_LOGGER_H_
//#define _TRACE_LOGGER_H_

#ifndef LOGGER
#define LOGGER 0
#endif

#ifdef TRACE_LOGGER
	#if LOGGER
	#define LOG_TRACE() TraceLogger logger(__FILE__, __FUNCTION__, __LINE__);
	#else
	#define LOG_TRACE()
	#endif
#else
	#define LOG_TRACE()
#endif

#ifndef LIB_DEBUG_TRACELOGGER_H_
#define LIB_DEBUG_TRACELOGGER_H_

#include <cstdio>
#include <iostream>

using namespace std;

class TraceLogger {
public:
	TraceLogger(
			const char* fileName,
			const char* funcName,
			int lineNumber);
	virtual ~TraceLogger();

	void msg(
			const char * msg);

private:
	const char* _fileName;
	const char* _funcName;

	static std::string Indent;
};

#endif /* LIB_DEBUG_TRACELOGGER_H_ */
