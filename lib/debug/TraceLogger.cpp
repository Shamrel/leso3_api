/*
 * TraceLogger.cpp
 *
 *  Created on: 18 мая 2016 г.
 *      Author: Lermash
 */

//#define TRACE_LOGGER

#ifdef TRACE_LOGGER

#include "TraceLogger.h"

std::string TraceLogger::Indent;

TraceLogger::TraceLogger(
		const char* fileName,
		const char* funcName,
		int lineNumber
		)
{
	_fileName = fileName;
	_funcName = funcName;
	std::cout << Indent << "---> "
			  << _funcName << "() - (" << _fileName
			  << ":" << dec << lineNumber << ")" << std::endl;
	Indent.append("    ");
}

TraceLogger::~TraceLogger() {
	Indent.resize(Indent.length() - 4);
	std::cout << Indent << "<--- " << _funcName << "() - (" << _fileName << ")" << std::endl;
}

void TraceLogger::msg(
		const char * msg)
{
	std::cout << Indent << msg << endl;
}


#endif
