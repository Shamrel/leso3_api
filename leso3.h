/*
 * leso3.h
 *
 *  Created on: 2 мая 2016 г.
 *      Author: Lermash
 */

#ifndef LESO3_H_
#define LESO3_H_


#include <stdint.h>

//#ifdef _WINDOWS
#if defined (_WIN32) || defined( _WIN64)
#ifdef BUILDING_LESO_LIB_DLL
#define LESO_DLL __declspec(dllexport) __cdecl
#else
#define LESO_DLL __declspec(dllimport)
#endif
#else
#define LESO_DLL
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define VERSION_MAJOR	(00)
#define VERSION_MINOR	(02)
#define VERSION_BUILD	(01)

#define LESO3_INVALID_INDEX (Leso3Index)(-1)

typedef int Leso3Index;

#define POSSIBLE_FIRMWARE_VERSION	\
	{								\
		0x0102,						\
		0x0103,						\
		0x0205						\
	};

#define COMPATIBLE_FIRMWARE_VERSION	\
	{								\
		0x0103						\
	};


typedef enum {
		LESO_OK,
		LESO_DEVICE_NOT_FOUND,
		LESO_DEVICE_NOT_OPENED,
		LESO_IO_ERROR,
		LESO_INSUFFICIENT_RESOURCES,
		LESO_INVALID_PARAMETER,
		LESO_INVALID_HEXFILE,
		LESO_ERROR_UPDATE_FIRMWARE,
		LESO_ERROR_CALIBRATION,
		LESO_NOT_SUPPORTED,
		LESO_OTHER_ERROR
	} LesoStatus;


typedef	enum {V1, V2, A1, A2, E1, E2} Leso3Entity;


typedef int	LESO_STATUS;

/**
 *  \brief Возвращает список всех последовательных портов в системе.
 *  \detail Названия портов разделены символом переноса.
 *  \param[out] ports Указатель на строку с именами портов.
 *  \param[in] bytesToRead Число байт, выделенных под строку с портами.
 *  \param[out] bytesReturned Число байт в строке с именами портов.
 *  \return статус операции.
 */
LESO_DLL LESO_STATUS leso3GetSerialPortsAll(
	char *ports,
	const unsigned int bytesToRead,
	unsigned int *bytesReturned);

/**
 *  \brief Возвращает список последовательных портов 
 *  на которых найден стенд.
 *  \detail Названия портов разделены символом переноса.
 *  \param[out] ports Указатель на строку с именами портов.
 *  \param[in] bytesToRead Число байт, выделенных под строку с портами.
 *  \param[out] bytesReturned Число байт в строке с именами портов.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3GetSerialPorts(
		char *ports,
		const unsigned int bytesToRead,
		unsigned int *bytesReturned
		);

/**
 *  \brief Возвращает версию драйвера.
 *  \param[out] driverVersion Указатель на переменную-контейнер.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3GetDriverVersion(
		uint32_t *driverVersion
		);

/**
 *  \brief Открывает для работы устройство.
 *  \detail Производит сброс и инициализацию.
 *  \param[in] serialPortName Указатель на строку с именем последовательного порта.
 *  \param[out] leso3Index Индекс открытого устройства. Исполььзуется в дальнейшем
 *  для обращения к устройству.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3Open(
		const char *serialPortName,
		int *leso3Index
		);

/**
 *  \brief Закрывает устройство.
 *  \param[in] leso3Index Индекс устройства.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3Close(
		int leso3Index
		);

/**
 *  \brief Возвращает измеренные значения.
 *  \detail Значения с учетом калибровочных коэффициентов.
 *  \param[in] leso3Index Индекс устройства.
 *  \param[out] value Указатель на массив. Память должна быть выделена заранее.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3GetMeasuredValue(
		int leso3Index,
		double *value
		);

/**
 *  \brief Возвращает измеренные значения.
 *  \detail Значения непосредственно с АЦП.
 *  \param[in] leso3Index Индекс устройства.
 *  \param[out] value Указатель на массив. Память должна быть выделена заранее.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3GetAdcValue(
		int leso3Index,
		uint16_t *value
		);

/**
 *  \brief Устанавливает напряжение на источниках.
 *  \param[in] leso3Index Индекс устройства.
 *  \param[in] ae Номер канала. Допустимые значения: E1, E2.
 *  \param[in] value Значение напряжения в Вольтах.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3SetSourseVoltage(
		int leso3Index,
		Leso3Entity ae,
		double value
		);

/**
 *  \brief Устанавливает напряжение на источниках.
 *  \detail Устанавливает значение непосредственно на ЦАП.
 *  \param[in] leso3Index Индекс устройства.
 *  \param[in] ae Номер канала. Допустимые значения: E1, E2.
 *  \param[in] value Значение ЦАП.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3SetDacValue(
		int leso3Index,
		Leso3Entity ae,
		uint16_t value
		);

/**
 *  \brief Устанавливает диапазон для выбранного канала.
 *  \detail В связи с аппаратным ограничением, изменение диапазона
 *  доступно только для мили амперметров (A1, A2).
 *  \param[in] leso3Index Индекс устройства.
 *  \param[in] ae Номер канала. Допустимые значения: A1, A2.
 *  \param[in] range Номер диапазона.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3SetRange(
		int leso3Index,
		Leso3Entity ae,
		int range
		);

/**
 *  \brief Возвращает номер версии прошивки.
 *  \param[in] leso3Index Индекс устройства.
 *  \param[out] version Номер версии.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3GetFirmwareVersion(
		int leso3Index,
		unsigned int *version
		);

/**
 *  \brief Обновляет прошивку устройства.
 *  \param[in] leso3Index Индекс устройства.
 *  \param[in] hexFile Указатель на строку с именем hex-файла.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3FirmwareUpdate(
		int leso3Index,
		const char *hexFile
		);

/**
 *  \brief Возвращает калибровочные коэффициенты для указанного канала
 *  \param[in] leso3Index Индекс устройства.
 *  \param[in] ae Номер канала.
 *  \param[out] gain Коэффициент усиления.
 *  \param[out] gain Коэффициент смещения нуля.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3GetCalibrationFactor(
		int leso3Index,
		Leso3Entity ae,
		int32_t *gain,
		int32_t *offset
		);

/**
 *  \brief Устанавливает калибровочные коэффициенты для указанного канала
 *  \param[in] leso3Index Индекс устройства.
 *  \param[in] ae Номер канала.
 *  \param[in] gain Коэффициент усиления.
 *  \param[in] gain Коэффициент смещения нуля.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3SetCalibrationFactor(
		int leso3Index,
		Leso3Entity ae,
		int32_t gain,
		int32_t offset
		);

/**
 *  \brief Проверяет статус устройства.
 *  \detail Посылает запрос устройству, если устройство подключено и
 *  в рабочем состаянии, то получает отклик.
 *  \param[in] leso3Index Индекс устройства.
 *  \param[out] ok Статус устройства. Если 0 -- устройство не ответило.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3TestDevice(
		int leso3Index,
		int *ok
		);

/**
 *  \brief Устанавливает импульсный режим измерения.
 *  \detail В импульсном режиме на источниках напряжения установлено
 *  нулевое значение. Непосредственно перед измерением на ЦАП подается
 *  требуемое значение, после измерения, значение снимается.
 *  \param[in] leso3Index Индекс устройства.
 *  \param[in] ok Включить режим.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3SetImpulsMode(
		int leso3Index,
		int ok
		);

/**
 *  \brief Аппаратный сброс устройства.
 *  \param[in] leso3Index Индекс устройства.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3Reset(
		int leso3Index
		);

/**
 *  \brief Возвращает информацию о последней ошибке.
 *  \param[out] LesoStatus Статус последней операции.
 *  \param[out] message Указатель на сообщение об ошибке.
 *  \param[in] messageLength Размер выделенной под сообщение памяти.
 *  Рекомендуется выделять не менее 30 байт.
 *  \param[out] index Индекс устройства с которым ассоциируется
 *  последняя ошибка. Если значение равно LESO3_INVALID_INDEX, то
 *  ошибка имеет общий характер.
 *  \return Статус операции.
 */
LESO_DLL LESO_STATUS leso3GetLastError(
		LesoStatus *status,
		char *message,
		unsigned int messageLength,
		int *index
		);

#ifdef __cplusplus
}
#endif

#endif /* LESO3_H_ */
