OBJECTS = $(CPP_SOURCES:.cpp = .o)

CC = g++

INC_DIR = ./include
SRC_DIR = ./src
LIB_DIR = ./lib

INCLUDES = -I $(INC_DIR) -I $(LIB_DIR)/serialib/ -I $(LIB_DIR)/debug/ -I.

CPP_SOURCES = $(SRC_DIR)/LESO3_export.cpp
CPP_SOURCES += $(LIB_DIR)/serialib/serialib.cpp 
#CPP_SOURCES += $(LIB_DIR)/debug/TraceLogger.cpp 
CPP_SOURCES += $(SRC_DIR)/LESO3Device.cpp 
CPP_SOURCES += $(SRC_DIR)/Leso3Measuring.cpp
CPP_SOURCES += $(SRC_DIR)/firmware.cpp
CPP_SOURCES += $(SRC_DIR)/LesoErrors.cpp

ifeq ($(OS),Windows_NT)
	OUTPUT = libLESO3.dll
else
	OUTPUT = libLESO3.so
endif


LIB = -ldl 
CFLAGS = -Wall -std=c++0x -static-libstdc++ -static-libgcc
#CFLAGS += -DLOGGER

all: $(OUTPUT) 
$(OUTPUT) : $(OBJECTS)
ifeq ($(OS), Windows_NT)
	$(CC) -shared -o $(OUTPUT) $(OBJECTS) $(INCLUDES) $(CFLAGS) -D BUILDING_LESO_LIB_DLL
else
	$(CC) -shared -o $(OUTPUT) $(OBJECTS) $(INCLUDES) $(CFLAGS) -fPIC
endif

clean:
	rm -rfv $(CPP_SOURCES:.cpp=.o) $(OUTPUT)