/*
 * hexfile.h
 *
 *  Created on: 8 мая 2016 г.
 *      Author: Shamrel
 */

#ifndef HEXFILE_H_
#define HEXFILE_H_

#include "LesoErrors.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;
/**
 \struct HexFileString
 \brief Структура хранит параметры строки hex-файла, согласно "INTEL HEX FORMAT".
 */
struct HexFileString {

	string str;						//!< Строка полностью в ASCII.
	uint8_t len; 					//!< Длина записи.
	uint16_t load_addr;				//!< Начальный адрес записи.
	uint8_t type;					//!< Тип строки: 0 -- данные для записи; 1 -- конец файла.
	vector <uint8_t>  data;			//!< Данные для записи.
	uint8_t crc;					//!< Контрольная сумма.
};

class Firmware
{
	//! Заполняет поля параметров
	int parse_hex_string (HexFileString &hexStringParam);
	uint8_t checksum(char *data);

public:
	enum : char {
			ACK = 0x06,
			NAK = 0x07
			};

	void loadHexFile(
			const char * fileName
			);

	int printHexFile(
			);

	int createWPack(
			char *pack,
			HexFileString &hexStringParam
			);

	int createErasePack(
			char *pack,
			int memoryType
			);

	int createInterrogatePack(
			char *pack
			);

	vector <HexFileString> hexFileString;
};

/**
16-bit address-field format, for files 64k bytes in length or less.

  DATA RECORD
  Byte 1	Header = colon(:)
  2..3		The number of data bytes in hex notation
  4..5		High byte of the record load address
  6..7		Low byte of the record load address
  8..9		Record type, must be "00"
  10..x	Data bytes in hex notation:
	x = (number of bytes - 1) * 2 + 11
  x+1..x+2	Checksum in hex notation
  x+3..x+4	Carriage return, line feed

  END RECORD
  Byte 1	Header = colon (:)
  2..3		The byte count, must be "00"
  4..7		Transfer-address (usually "0000")
		the jump-to address, execution start address
  8..9		Record type, must be "01"
  10..11	Checksum, in hex notation
  12..13	Carriage return, line feed
*/

#endif /* HEXFILE_H_ */
