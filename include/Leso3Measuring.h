/*
 * Leso3Measuring.h
 *
 *  Created on: 23 мая 2016 г.
 *      Author: Lermash
 */

#ifndef SRC_LESO3MEASURING_H_
#define SRC_LESO3MEASURING_H_

#include "leso3.h"
#include "Leso3Device.h"

#include <cstdio>
#include <iostream>
#include <vector>
#include <string>

using std::cout;
using std::vector;
using std::string;

class Leso3Measuring  //:  public LesoErrors
{

	Serialib _serial;
	vector <Leso3Device> _devices;

	vector <uint16_t> _possibleFirmwareVersion = POSSIBLE_FIRMWARE_VERSION;
	vector <uint16_t> _compatibleFirmwareVersion = COMPATIBLE_FIRMWARE_VERSION;

public:


	Leso3Device &operator[](int pos);
	const Leso3Device &operator[](int pos) const;

	Leso3Measuring();
	~Leso3Measuring();


	int searchDevices(
				vector <string> &serialPortAll);

	int searchDevices(
			vector <string> &serialPortAll,		// Возвращает все порты.
			vector <string> &serialPortLeso3);	// Возвращает порты, на которых найден LESO3

	bool isLeso3Device(
			Leso3Index index
			);
	bool isLeso3Device(
			string &serialPortName
			);
	bool isLeso3Device(
			const char *serialPortName
			);

	Leso3Index openDevice(
			string &serialPortName
			);

	Leso3Index openDevice(
			const char *serialPortName
			);
};


#endif /* SRC_LESO3MEASURING_H_ */
