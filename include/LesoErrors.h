
/**
\file 

\author Shauerman Alexander <shamrel@labfor> 
На основе кода Ryasanov Ilya <ryasanov@gmail.com> 
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 11.05.2016
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

/*! 
	Класс который представляет контейнер и хранит сообщение и тип ошибки
	Сообщение об ошибки хранится в структуре типа ErrorMessage
	Следует заметить, что использование всех полей ErrorMessage необязательно,
	какие поля записываем, в структуру, такие и читаем когда возникнет Exception
*/

#ifndef _LESO_ERROR_H
#define _LESO_ERROR_H

#include "leso3.h"

#include <string>

using std::string;

#define ERROR_MSG(m) (m), (__FILE__), (__FUNCTION__)


class LesoErrors
{

	struct ErrorMessage
	{
		LesoStatus errorCode;						//!< Тип ошибки
		string message;								//!< Сообщение об ошибки
		string fileName;							//!< Название файла в котором произошла ошибка
		string functionName;						//!< Название функции в которой произошла ошибка
	};

	ErrorMessage  _report;
	Leso3Index _channel = LESO3_INVALID_INDEX;	//!< Номер канала, в котором произошла ошибка

public:

	LesoErrors();

	/*!
		\param[in] error_message Сообщение о ошибке
		\param[in] function_name Имя функции, где возник сбой
		\param[in] e_status Переменная возвращенная функцией
	*/
	LesoErrors(
			const char *errorMessage,
			const char *fileName,
			const char *functionName,
			LesoStatus errorCode = LESO_OTHER_ERROR,
			Leso3Index channel	= LESO3_INVALID_INDEX
			);

	void setError(
			const char *errorMessage,
			const char *fileName,
			const char *functionName,
			LesoStatus errorCode = LESO_OTHER_ERROR,
			Leso3Index channel	= LESO3_INVALID_INDEX
			);
	void setError(
			Leso3Index channel = LESO3_INVALID_INDEX
			);
	void setError(
			ErrorMessage error,
			Leso3Index channel = LESO3_INVALID_INDEX
			);

	/*!
		Функция возвращает имя функции, где произошел сбой
		\return указатель на имя функции, где возникла ошибка
	*/
	 const  string &getErrorFunctionName();

	 const  string &getErrorFileName();

	 const ErrorMessage &getError();

	/*!
		Получить статус который возвратила функция

		\return обычная целочисленное значение
	*/
    LesoStatus getErrorCode();

	/*!
		Получить доступ к переданной структуре

		\return указатель на структуру, где информация о сбое
	*/
    const string & getErrorMessage();

    Leso3Index getErrorChannel();

};

#endif
