
#ifndef _LESO3DEVICE_H_
#define _LESO3DEVICE_H_

#include "firmware.h"
#include "serialib.h"
#include "LesoErrors.h"

#include <map>

using std::cout;
using std::vector;
using std::string;



#define LESO3_RUN_BAUD_RATE 460800
#define LESO3_PROG_BAUD_RATE 9600
#define READ_TIMEOUT_MS 100
#define MAX_PORT_NAME_LENGTH 20

#define RESET_DELAY_MS	(20)



class Leso3Device // : public LesoErrors
{

private:


	Firmware _firmware;
	string _currentPortName;
	bool _opened = false;
	Serialib _serial;
	// перечень команд обмена данными
	enum _Leso3Cmd: uint8_t {
		CMD_ABOUT		=	0x30,		// получить версию прошивки
		CMD_DAC0		=	0x31,		// установить напряжение на ЦАП0
		CMD_DAC1		=	0x32,		// установить напряжение на ЦАП1
		CMD_ADC			=	0x33,		// измерить напряжения и токи
		CMD_NOP			=	0x40,		// тест связи
		CMD_EEP_WR		=	0x41,		// записать данные с ПК в EEPROM
		CMD_EEP_RD		=	0x42,		// ситать данные из EEPROM и отправить ПК

		CMD_RELAY1_ON	=	0x50,		// включить/выключить реле миллиамперметров
		CMD_RELAY1_OFF	=	0x51,
		CMD_RELAY2_ON	=	0x52,
		CMD_RELAY2_OFF	=	0x53,
		CMD_IMPULSE_OFF	=	0x54,		// выключить импульсный режим измерения
		CMD_IMPULSE_ON	=	0x55		// включить импульсный режим измерения
	};

public:


	// Режим работы LESO3. Опция при открытии устройства
	enum Leso3RunMode {
		MODE_RUN,		// Нормальный режим работы.
		MODE_PROG		// Режим программирования.
	};

//	enum Entity: int {V1, V2, A1, A2, E1, E2};

	//! TODO сделать private
	struct AnalogEntity
	{
		uint16_t code;
		double value;
		int range;
		int32_t gain;
		int32_t offset;
	};

	//! TODO сделать private
	map <Leso3Entity, AnalogEntity> analogEntity =
	{	{V1, {0}},
		{V2, {0}},
		{A1, {0}},
		{A2, {0}},
		{E1, {0}},
		{E2, {0}}, };

	Leso3Device();
	Leso3Device(const char * portName);
	Leso3Device(string &portName);

	~Leso3Device();

	Leso3Device (const Leso3Device&) = delete; 	//запретить копирование

	Leso3Device(Leso3Device&& that); 			// Разрешить перемещение

	void operator=(const Leso3Device&) = delete; //запретить присваивание

	Leso3Device& operator=(Leso3Device&& that);

	bool open(
			string serialPortName,
			uint32_t baudRate = LESO3_RUN_BAUD_RATE,
			Leso3RunMode mode = MODE_RUN);

	bool open(
			uint32_t baudRate = LESO3_RUN_BAUD_RATE,
			Leso3RunMode mode = MODE_RUN);

	bool isOpened();

	void close();

	void reset();

	void setRange(Leso3Entity ch, int range);

	void setImpulsMode(bool mode);

	uint16_t getFirmwareVersion();

	void firmwareUpdate(string  fileName);

	void getAdcValue(vector <uint16_t> &value);
	void getAdcValue(vector <double> &value);

	void setDacValue(Leso3Entity dac, uint16_t value);
	void setDacValue(Leso3Entity dac, double value);

	string &getPortName();

	void getCalibrationFactor(Leso3Entity ent, int32_t &gain, int32_t &offset);

	void setCalibrationFactor(Leso3Entity ent, int32_t gain, int32_t offset);

	bool test();
private:

	void _performAdc();

	void _readCalibrationFactor();

	void _writeCalibrationFactor(Leso3Entity ent);

	void _getEeprom(char *value, uint16_t len, uint16_t offset = 0);

	void _setEeprom(char *value, uint16_t len, uint16_t offset = 0);

	void _setRunMode(Leso3RunMode mode = MODE_RUN);

	void _delay_ms(int delay_ms);

	bool _isFirmwareActual();

	LesoStatus _serialStatusConvert(int serialStatus);

	inline uint16_t _charToInt(char a, char b);
	inline uint32_t _charToInt(char a, char b, char c, char d);
	inline uint32_t _charToInt(char *a);
	
};

#endif // _LESO3DEVICE_H_
