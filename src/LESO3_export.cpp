/*
 * LESO3_export.cpp
 *
 *  Created on: 20 мая 2016 г.
 *      Author: Lermash
 */

#include "leso3.h"
#include "Leso3Measuring.h"

using std::string;

LesoErrors lastError;

Leso3Measuring measure;


LESO_DLL LESO_STATUS leso3GetSerialPortsAll(
	char *ports,
	const unsigned int bytesToRead,
	unsigned int *bytesReturned)
{
	vector <string> portNameAll;
	vector <string> portNameLeso3;

	try	{

		measure.searchDevices(portNameAll, portNameLeso3);

		string str;
		for (size_t i = 0; i < portNameAll.size(); i++ ) {
			str += portNameAll[i] + "\n";
		}
		str += "\0";
		if (str.length() > bytesToRead)
			return LESO_INSUFFICIENT_RESOURCES;


		*bytesReturned = (int)str.length();
		str.copy(ports, str.length());
	}
	catch (LesoErrors &e) {
		lastError.setError(e.getError());
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"));
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
}

LESO_DLL LESO_STATUS leso3GetSerialPorts(
	char *ports,
	const unsigned int bytesToRead,
	unsigned int *bytesReturned)
{
	vector <string> portNameAll;
	vector <string> portNameLeso3;
	try	{
		measure.searchDevices(portNameAll, portNameLeso3);

		string str;
		for (size_t i = 0; i < portNameLeso3.size(); i++ ) {
			str += portNameLeso3[i] + "\n";
		}
		str += "\0";
		if (str.length() > bytesToRead)
			return LESO_INSUFFICIENT_RESOURCES;

		*bytesReturned = (int)str.length();
		str.copy(ports, str.length());
	}
	catch (LesoErrors &e) {
		lastError.setError(e.getError());
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"));
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
}

LESO_DLL LESO_STATUS leso3GetDriverVersion(uint32_t *driverVersion)
{
	*driverVersion =	((uint32_t)VERSION_MAJOR << 16) |
						((uint32_t)VERSION_MINOR << 8) |
						VERSION_BUILD;
	return LESO_OK;
}

LESO_DLL LESO_STATUS leso3Open(
	const char *serialPortName, int *leso3Index)
{
	try	{
		*leso3Index = measure.openDevice(serialPortName);
	}
	catch (LesoErrors &e) {
		lastError.setError(e.getError());
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"));
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3Close(
	int leso3Index)
{
	try	{
		measure[leso3Index].close();
	}
	catch (LesoErrors &e) {
		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3GetMeasuredValue(
		int leso3Index,
		double *value
		)
{
	try	{
		vector <double> val;
		measure[leso3Index].getAdcValue(val);
		for (auto v : val)
			*(value++) = v;

	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3GetAdcValue(
		int leso3Index,
		uint16_t *value
		)
{
	try	{
		vector <uint16_t> val;
		measure[leso3Index].getAdcValue(val);
		for (auto v : val)
			*(value++) = v;

	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3SetSourseVoltage(
		int leso3Index,
		Leso3Entity ae,
		double value
		)
{
	try	{

		measure[leso3Index].setDacValue(ae,  value);

	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3SetDacValue(
		int leso3Index,
		Leso3Entity ae,
		uint16_t value
		)
{
	try	{

		measure[leso3Index].setDacValue(ae,  value);

	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};


LESO_DLL LESO_STATUS leso3SetRange(
		int leso3Index,
		Leso3Entity ae,
		int range
		)
{
	try	{

		measure[leso3Index].setRange(ae,  range);
	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3GetFirmwareVersion(
		int leso3Index,
		unsigned int *version
		)
{
	try	{
		*version = measure[leso3Index].getFirmwareVersion();
	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3FirmwareUpdate(
		int leso3Index,
		const char *hexFile
		)
{
	try	{
		measure[leso3Index].firmwareUpdate(string(hexFile));
	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3GetCalibrationFactor(
		int leso3Index,
		Leso3Entity ae,
		int32_t *gain,
		int32_t *offset
		)
{
	try	{
		if ((ae < V1) && (ae > E2))
			throw LesoErrors(ERROR_MSG("Incorrect Entity"), LESO_INVALID_PARAMETER);

		measure[leso3Index].getCalibrationFactor(ae, *gain, *offset);
	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3SetCalibrationFactor(
		int leso3Index,
		Leso3Entity ae,
		int32_t gain,
		int32_t offset
		)
{
	try	{
		if ((ae < V1) && (ae > E2))
			throw LesoErrors(ERROR_MSG("Incorrect Entity"), LESO_INVALID_PARAMETER);

		measure[leso3Index].setCalibrationFactor(ae, gain, offset);
	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3TestDevice(
		int leso3Index,
		int *ok
		)
{
	try	{
		*ok = measure[leso3Index].test();
	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3SetImpulsMode(
		int leso3Index,
		int ok
		)
{
	try	{
			measure[leso3Index].setImpulsMode(ok);
		}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

		return LESO_OK;
};


LESO_DLL LESO_STATUS leso3Reset(
		int leso3Index
		)
{
	try	{
		measure[leso3Index].reset();
	}
	catch (LesoErrors &e) {
		if (e.getErrorCode() == LESO_IO_ERROR)
			measure[leso3Index].close();

		lastError.setError(e.getError(), leso3Index);
		return e.getErrorCode();
	}
	catch (...)	{
		lastError.setError(ERROR_MSG("Unknown error"),LESO_OTHER_ERROR, leso3Index);
		return LESO_OTHER_ERROR;
	}

	return LESO_OK;
};

LESO_DLL LESO_STATUS leso3GetLastError(
		LesoStatus *status,
		char *message,
		unsigned int messageLength,
		int *index
		)
{

	if (messageLength < lastError.getErrorMessage().length())
		return LESO_INSUFFICIENT_RESOURCES;

	::strncpy(message,
			lastError.getErrorMessage().c_str(),
			lastError.getErrorMessage().length()
			);

	*status = lastError.getErrorCode();
	*index = lastError.getErrorChannel();
	return LESO_OK;
};
