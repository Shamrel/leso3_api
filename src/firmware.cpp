/*
 * hexfile.cpp
 *
 *  Created on: 9 мая 2016 г.
 *      Author: Lermash
 */
#include <firmware.h>
#include <sstream>


void Firmware::loadHexFile(const char * fileName)
{

	try	{
		ifstream hexfile(fileName);
		if(!hexfile.is_open())
			throw LesoErrors(ERROR_MSG("Error opening hexfile"), LESO_INVALID_HEXFILE);

		char str[80];             // статический буфер строки
		while (!hexfile.getline(str, sizeof(str)).eof())
		{
			HexFileString strParam;
			strParam.str = str;
			parse_hex_string(strParam);
			hexFileString.push_back(strParam);
		}
		hexfile.close();
	}
	catch (LesoErrors &) {
		throw ;
	}
	catch(...) {
		throw LesoErrors(ERROR_MSG("Unknown error hexfile"), LESO_INVALID_HEXFILE);
	}
};


/**
\brief Функция разбирает строку hex-файла на параметры.
\details Функция сканирует строку hex-файла, выделяет адрес записи,
число байт данных, проверяет контрольную сумму. Полученными результатами
функция заполняет структуру param.
\param str Указатель на строку для анализа
\param param Структура, содержащая параметры и данные строки. Выходные данные.
\throw -1 Строка не соответсвует INTEL HEX FORMAT.
\throw -2 Ошибка контрольной суммы.
*/

int Firmware::parse_hex_string (HexFileString &hexStringParam)
{
	uint8_t crc = 0;								// Аккумулятор для контрольной суммы.

	stringstream strStream;
	strStream.unsetf(ios::dec);
	strStream.setf(ios::hex);
	vector <uint8_t> str;

	string::iterator p = hexStringParam.str.begin();
	if (*(p++) != ':')
		throw LesoErrors(ERROR_MSG("Invalid hexfile"), LESO_INVALID_HEXFILE);

	int tmp;
	bool second = false;
	while ((p) != hexStringParam.str.end())
	{
		strStream << *(p++);
		if (second)
		{
			strStream << ' ';
			strStream >> tmp;
			str.push_back((uint8_t)tmp);
			crc += tmp;
		}
		second = !second;
	}

	if (crc)
		throw LesoErrors(ERROR_MSG("Hexfile CRC error"), LESO_INVALID_HEXFILE);

	if (str.size() < 5)
		throw LesoErrors(ERROR_MSG("Invalid hex string len"), LESO_INVALID_HEXFILE);

	vector<uint8_t>::iterator d = str.begin();

	hexStringParam.len = *(d++);

	hexStringParam.load_addr = ((*d) << 8) | *(d+1);
	d +=2;

	hexStringParam.type = *(d++);

	hexStringParam.data.insert(hexStringParam.data.begin(),	d , str.end()-1);

	return 0;
}

/**
\brief Функция выводит в cout содержимое файла.
*/
int Firmware::printHexFile()
{
	cout << "len\taddr\ttype\tdata" << endl;
	for (size_t i = 0; i < hexFileString.size(); i++ )
	{
		cout << hex
			 << (int)hexFileString[i].len << '\t'
			 << (int)hexFileString[i].load_addr << '\t'
			 << (int)hexFileString[i].type << '\t';
		for (size_t j = 0; j < hexFileString[i].data.size(); j++ )
			cout << hex << (int)hexFileString[i].data[j] << ' ';
		cout << endl;
	}
	return 0;
};

uint8_t Firmware::checksum(char *data)
{
	uint8_t i, crc = 0;
	for (i=0;i <= data[0]; i++ )
		crc -= data[i];

	return crc;
}

/**
\brief Функция формирует пакет для отправки микроконтроллеру.
\details Пакет содержит информацию для записи в память программ или данных.
Формируется пакет на основании параметров, полученных из одной строки hex-файла.
\param pack Указатель на буфер символов, в котором должен быть сформирован пакет.
\param param Структура, содержащая параметры и данные для передачи.
\return Число байт для отправки.
*/
int Firmware::createWPack(char *pack, HexFileString &hexStringParam)
{
	int ptr = 0;
	pack[ptr++] = 0x07;									// магическое число
	pack[ptr++] = 0x0E;									// магическое число
	pack[ptr++] = hexStringParam.len + 4;				// число байт данных
	pack[ptr++] = 'W';									// идентификатор пакета
	pack[ptr++] = 0;									// адрес страницы
	pack[ptr++] = (uint8_t)0xFF&(hexStringParam.load_addr>>8);	// старший байт адреса
	pack[ptr++] = (uint8_t)0xFF&(hexStringParam.load_addr);		// младший байт адреса

	int i;
	for (i = 0; i < hexStringParam.len; i++)
	{
		pack[ptr++] = hexStringParam.data[i];
	}
	pack[ptr++] = checksum(&pack[2]);
	return ptr;
};

/**
\brief Функция формирует пакет с командой для очистки памяти ADuC.
\param pack Указатель на буфер символов, в котором должен быть сформирован пакет.
\param memoryType	memoryType == 0, чистим всю память;
					memoryType == 1, чистим только память программ.
\return Число байт для отправки.
*/
int Firmware::createErasePack(char *pack, int memoryType )
{
	uint8_t ptr = 0;
	pack[ptr++] = 0x07;					// магическое число
	pack[ptr++] = 0x0E;					// магическое число
	pack[ptr++] = 1;					// число байт данных
	pack[ptr++] = memoryType?'C':'A';	// идентификатор пакета
	pack[ptr++] = checksum(&pack[2]);
	return ptr;
}

/**
\brief Функция формирует пакет с командой для запроса информации об встроенном загрузчике.
\param pack Указатель на буфер символов, в котором должен быть сформирован пакет.
\return Число байт для отправки.
*/
int Firmware::createInterrogatePack(char *pack )
{
	uint8_t ptr = 0;
	pack[ptr++] = '!';
	pack[ptr++] = 'Z';
	pack[ptr++] = 0;
	pack[ptr++] = 0xA6;
	return ptr;
}
