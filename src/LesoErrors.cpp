/**
\file

\author Shauerman Alexander <shamrel@labfor>
На основе кода Ryasanov Ilya <ryasanov@gmail.com>
\brief Библиотека предоставляющая API для взаимодействия с анализатором сигналов LESO4
\details 
\version 0.3
\date 11.05.2016
\copyright
Это программное обеспечение распространяется под лицензией BSD 2-ух пунктов.
Эта лицензия дает все права на использование и распространение программы в
двоичном виде или в виде исходного кода, при условии, что в исходном коде
сохранится указание авторских прав.
This software is licensed under the simplified BSD license. This license
gives everyone the right to use and distribute the code, either in binary or
source code format, as long as the copyright license is retained in
the source code.
*/

#include "LesoErrors.h"

LesoErrors::LesoErrors()
{
	_report.errorCode = LESO_OTHER_ERROR;
	_channel = LESO3_INVALID_INDEX;
};

LesoErrors::LesoErrors(
		const char *errorMessage,
		const char *fileName,
		const char *functionName,
		LesoStatus errorCode,
		Leso3Index channel)
{	
	_report.fileName = fileName,
	_report.errorCode = errorCode;
	_report.message = errorMessage;
	_report.functionName = functionName;
	_channel = channel;
};

void LesoErrors::setError(
		const char *errorMessage,
		const char *fileName,
		const char *functionName,
		LesoStatus errorCode,
		Leso3Index channel)
{
	_report.errorCode = errorCode;

	_report.message.clear();
	_report.message = errorMessage;

	_report.functionName.clear();
	_report.functionName = functionName;

	_channel = channel;
};

void LesoErrors::setError(
		Leso3Index channel
		)
{
	_channel = channel;
};

void LesoErrors::setError(
		ErrorMessage error,
		Leso3Index channel
		)
{
	_report = error;
	_channel = channel;
};

const LesoErrors::ErrorMessage & LesoErrors::getError()
{
	return _report;
};

Leso3Index LesoErrors::getErrorChannel()
{
	return _channel;
};

const string & LesoErrors::getErrorFunctionName()
{
	return _report.functionName;
};

const string & LesoErrors::getErrorFileName()
{
	return _report.fileName;
};

LesoStatus LesoErrors::getErrorCode()
{
	return _report.errorCode;
};

const string & LesoErrors::getErrorMessage()
{
	return _report.message;
};
