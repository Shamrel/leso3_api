/*
 * Leso3Measuring.cpp
 *
 *  Created on: 23 мая 2016 г.
 *      Author: Lermash
 */

#include "Leso3Measuring.h"
#define LOGGER  (0)
#include "TraceLogger.h"

Leso3Measuring::Leso3Measuring() {
	LOG_TRACE();
}

Leso3Measuring::~Leso3Measuring() {
	LOG_TRACE();
}

Leso3Device& Leso3Measuring::operator[](int pos)
{
	if ((pos < 0) || (pos >= (int)_devices.size()))
		throw LesoErrors(ERROR_MSG("Out of range"), LESO_INVALID_PARAMETER );

	return _devices.at(pos);
}

const Leso3Device& Leso3Measuring::operator[](int pos) const
{
	if ((pos < 0) || (pos >= (int)_devices.size()))
		throw LesoErrors(ERROR_MSG("Out of range"), LESO_INVALID_PARAMETER );

	return _devices.at(pos);
}

/**
 \return Количество последовательных портов в системе
 */
int Leso3Measuring::searchDevices(
			vector <string> &serialPortAll)
{
	return _serial.enumSerialPorts(serialPortAll);
};

/**
 \return Число найденных LESO3
 */
int Leso3Measuring::searchDevices(
			vector <string> &serialPortAll,
			vector <string> &serialPortLeso3 )
{
	searchDevices(serialPortAll);

	for (size_t i = 0; i < serialPortAll.size(); i++ )
		if (isLeso3Device(serialPortAll[i]))
			serialPortLeso3.push_back(serialPortAll[i]);
	return serialPortLeso3.size();
};

Leso3Index Leso3Measuring::openDevice(
		string &serialPortName)
{
	LOG_TRACE();
		// Проверяем, открыто ли уже устройство.
	for (size_t i = 0; i < _devices.size(); i++ )
		if (serialPortName == _devices[i].getPortName()) { 	// устройство уже есть в списке
			if (!_devices[i].isOpened()) {
				if (_devices[i].open())
					return (Leso3Index)i;
				else
					return LESO3_INVALID_INDEX;
			}
			else return (Leso3Index)i;
		}

	// Добавляем новое устройство в список
	_devices.emplace_back(serialPortName);
	return (Leso3Index)(_devices.size() -1);
}

Leso3Index Leso3Measuring::openDevice(
		const char *serialPortName)
{
	string str = serialPortName;
	return openDevice(str);
}


/** \addtogroup isLeso3Device
 *  @{
 */

bool Leso3Measuring::isLeso3Device(
		Leso3Index index)
{
	LOG_TRACE();

	if (index >= (Leso3Index)_devices.size())
		throw LesoErrors(ERROR_MSG("Incorrect index"), LESO_INVALID_PARAMETER);

	uint16_t version = 0;

	if (_devices.at(index).isOpened())
		version = _devices.at(index).getFirmwareVersion();
	else
	{
		_devices.at(index).open();
		version = _devices.at(index).getFirmwareVersion();
		_devices.at(index).close();
	}

	for (auto ver : _possibleFirmwareVersion)
		if ( version == ver)
			return true;

	return false;

}

/**
 *  \brief Определяет, подключено ли к указанному порту LESO3
 */
bool Leso3Measuring::isLeso3Device(
		string &serialPortName)
{
	try
	{
		Leso3Index index = LESO3_INVALID_INDEX;

		for (size_t i = 0; i < _devices.size(); i++)
			if (serialPortName == _devices[i].getPortName() ) {
				index = (Leso3Index)i;
				return isLeso3Device(index);
			}

		uint16_t version = 0;

		Leso3Device dev(serialPortName);
		version = dev.getFirmwareVersion();

		for (auto ver : _possibleFirmwareVersion)
			if (version == ver)
				return true;

		return false;

	}
	catch (LesoErrors &e) {
		return false;
	}
	catch (...)	{
		throw LesoErrors(ERROR_MSG("Unknown error"));
	}
}

bool Leso3Measuring::isLeso3Device(
		const char *serialPortName)
{
	string str = serialPortName;
	return isLeso3Device(str);
}

/** @}*/







