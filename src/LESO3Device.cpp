#include "Leso3Device.h"

//#include <new>


#define DEBUG 1
#define LOGGER  (0)
#include "TraceLogger.h"

using namespace std;
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
stringstream debug; //debug Stream;

#if DEBUG
#define ds cout
#else
#define ds debug
#endif


Leso3Device::Leso3Device()
{
	LOG_TRACE();
}

Leso3Device::Leso3Device(const char *portName)
{
	LOG_TRACE();
	string str = portName;
	open(str);
}

Leso3Device::Leso3Device(string &portName)
{
	LOG_TRACE();
	open(portName);
}

//Leso3Device::Leso3Device(const Leso3Device &obj)
//{
//	LOG_TRACE();
//};

Leso3Device::~Leso3Device()
{
	LOG_TRACE();
}

Leso3Device::Leso3Device (Leso3Device&& that)
{
//	cout << "Leso3Device mov\n";

	_currentPortName.clear();
	_currentPortName = that._currentPortName;
	that._currentPortName.clear();

	_serial = std::move(that._serial);
}

Leso3Device& Leso3Device::operator=(Leso3Device&& that)
{
	this->_currentPortName.clear();
	std::swap(this->_currentPortName, that._currentPortName);

	_serial = std::move(that._serial);

	return *this;
}

/**
 *   \return true если порт удалось открыть
 */
bool Leso3Device::open(string serialPortName, uint32_t baudRate, Leso3RunMode mode)
{

	LOG_TRACE();
	if (isOpened())	return true;
	try
	{
		_serial.open(serialPortName.c_str(), baudRate);
		_setRunMode(mode);
		reset();

		_currentPortName = serialPortName;
		_opened = true;
	}
	catch (Serialib::SerialPortStatus &e)
	{
//		if(e == Serialib::SERIAL_DEVICE_NOT_FOUND)
//			return false;
//		setError(ERROR_MSG("Serial: port not Found"), _serialStatusConvert(e));
		throw LesoErrors(ERROR_MSG("Serial: port not Found"), _serialStatusConvert(e));
	}
	return true;
}

bool Leso3Device::open(uint32_t baudRate, Leso3RunMode mode)
{
	LOG_TRACE();
	if (_currentPortName.empty())
		return false;
	return open(_currentPortName, baudRate, mode);
}


void Leso3Device::close()
{
	LOG_TRACE();
	try
	{
		_serial.close();
		_opened = false;
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: close error"), _serialStatusConvert(e));
	}
}

/**
 *  \brief Аппаратный сброс устройства.
 */
void Leso3Device::reset()
{
	LOG_TRACE();
	try
	{
		_serial.setDTR(true);
		_serial.purge();
		_delay_ms(RESET_DELAY_MS);
		_serial.setDTR(false);
		_delay_ms(RESET_DELAY_MS);
//		try
//		{
//			_readCalibrationFactor();
//		} // Если устройство не прошито, то ошибка возникнет.
//		catch (LesoErrors &e) {	}
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: IO error"), _serialStatusConvert(e));
	}
}

/**
 *  \brief Управление реле диапазонов амперметра.
 *  \param[in]	relayA1	Включает реле A1.
 *  \param[in]	relayA2	Включает реле A2.
 */
void Leso3Device::setRange(
		Leso3Entity ch,
		int range
		)
{

	char cmd[1] = {0};

	try
	{

		switch (ch)
		{
			case A1:
				cmd[0] = range ? CMD_RELAY1_OFF : CMD_RELAY1_ON;
				analogEntity.at(ch).range = range;
				break;
			case A2:
				cmd[0] = range ? CMD_RELAY2_OFF : CMD_RELAY2_ON;
				analogEntity.at(ch).range = range;
				break;
			default:
				throw LesoErrors(ERROR_MSG("This channel does not support the changing of the range"),
						LESO_NOT_SUPPORTED);
		}

		_serial.write(cmd, sizeof(cmd));
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: error"), _serialStatusConvert(e));
	}
};

/**
 *  \brief Включает/выключает импульсный режим измерения.
 *  \param[in]	mode	true -- режим активирован.
 */
void Leso3Device::setImpulsMode(bool mode)
{
	char cmd[1] = {0};

	cmd[0] = mode ? CMD_IMPULSE_ON : CMD_IMPULSE_OFF;

	try
	{
		_serial.write(cmd, sizeof(cmd));
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: error"), _serialStatusConvert(e));
	}
};

/**
 * \brief Возвращает версию прошивки.
 * \param[out] version.
 * \return LESO_OK  если устройство LESO3,
 * \return LESO_ERROR если это какое-либо другое устройство.
 */
uint16_t Leso3Device::getFirmwareVersion()
{
	LOG_TRACE();
	char cmd[1] = {CMD_ABOUT};
	char reciveBuff[2] = {0};

	try
	{
		_serial.write(cmd, 1);
		unsigned int bytesReceived;
		bytesReceived = _serial.read(reciveBuff, 2, READ_TIMEOUT_MS);
		if (bytesReceived < 2)
			return 0;
		return  (reciveBuff[0] << 8) | (reciveBuff[1]);
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: error"), _serialStatusConvert(e));
	}
	catch (...)
	{
		cout << "Unknown error" << endl;
		throw;
	}

	return 0;
}

/**
 *  \brief	Обновляет прошивку устройства.
 *  \detail Прошивка должна быть в формате hex-фала (Intel)/. Если порт был уже открыт,
 *  закрывает порт, после прошивки откроем его вновь.
 *  \param[in]	serialPortName	Имя последовательного порта.
 *  \param[in]	fileName	Полное имя hex-файла.
 */
void Leso3Device::firmwareUpdate(string  fileName)
{
	LOG_TRACE();
	try
	{
		_firmware.loadHexFile(fileName.c_str());

		string openedSerialPortName;

		if (!isOpened())
		{
			throw LesoErrors(ERROR_MSG("The port is not open"),
					LESO_DEVICE_NOT_OPENED);
		}
		close();
#if 1
		cout << boolalpha << "Open from firmware update: "
			 << open(LESO3_PROG_BAUD_RATE, MODE_PROG)
			 << endl;
#else
		open(LESO3_PROG_BAUD_RATE, MODE_PROG);
#endif

		_setRunMode(MODE_RUN);

		char rx_buff[64], tx_buff[64];
		int readBytes = _serial.read(rx_buff, 25, READ_TIMEOUT_MS);
#if DEBUG
		cout << "The response (" << "len = " << readBytes <<"): ";
		cout << rx_buff << endl;
#endif
		string reciv = rx_buff;
		if (reciv.find("ADI") == string::npos)
			throw LesoErrors(ERROR_MSG("Microcontroller does not respond"), LESO_ERROR_UPDATE_FIRMWARE);

		int packLen = _firmware.createErasePack(tx_buff, 1);
		_serial.write(tx_buff, packLen);

		char resp = 0;
		readBytes = _serial.read(&resp, 1, READ_TIMEOUT_MS);
		if ((readBytes != 1) || (resp != Firmware::ACK) )
			throw LesoErrors(ERROR_MSG("Error erasing memory"), LESO_ERROR_UPDATE_FIRMWARE);
#if DEBUG
		cout << "Memory erasing: OK (" << (int)resp << ")" << endl;
		int i = 0;
		cout << "String\t| type\t| len\t| resp" << endl;
		cout << "--------------------------------" << endl;
#endif
		for (auto str : _firmware.hexFileString)
		{
#if DEBUG
		cout << i++ << " \t|  "
			 << (int)str.type << " \t|  "
			 << (int)str.len;
#endif

			if (str.type)	// Последняя строка hexfile
				break;
			packLen = _firmware.createWPack(tx_buff, str);
			_serial.write(tx_buff, packLen);

			readBytes = _serial.read(&resp, 1, READ_TIMEOUT_MS);
#if DEBUG
			cout << " \t|  "<< (int)resp <<  endl;
#endif
			if ((readBytes != 1) || (resp != Firmware::ACK) )
				throw LesoErrors(ERROR_MSG("Error when sending string of the firmware"), LESO_ERROR_UPDATE_FIRMWARE);
		}
#if DEBUG
		cout << endl << " Update Ok "<< endl;
#endif
		close();	// Закрываем порт, открытый для программирования.
		open();		// Открываем порт в рабочем режиме.
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: error"), _serialStatusConvert(e));
	}

};

/**
 *  \brief	Возвращает значения АЦП.
 *  \detail Запускает преобразование АЦП.
 *  \param[out]	value	Вектор измеренных значений.
 */
void Leso3Device::getAdcValue(vector <uint16_t> &value)
{
	LOG_TRACE();
	value.clear();
	_performAdc();

	for (auto it = analogEntity.begin(); it != analogEntity.upper_bound(A2); ++it)
		value.push_back(it->second.code);
};

/**
 *  \brief	Возвращает значения измеренных величин.
 *  \detail Запускает преобразование АЦП. Если до этого вызова калибровочные
 *  коэффициенты не были считаны из устройства, то запращивает коэффициенты.
 *  Вычисляет значение измеряемых параметров, возвращает вектор значений.
 *  \param[out]	value	Вектор измеренных значений.
 */
void Leso3Device::getAdcValue(vector <double> &value)
{
	LOG_TRACE();
	value.clear();
	_performAdc();

	if (analogEntity.at(V1).offset == 0)
		_readCalibrationFactor();

	if (analogEntity.at(V1).offset == 0)
		throw LesoErrors(ERROR_MSG("Error read Coefficients"), LESO_ERROR_CALIBRATION);

	for (auto it = analogEntity.begin(); it != analogEntity.upper_bound(A2); ++it)
	{
		double divider = (it->second.range == 0) ? 1 : 100;
		it->second.value = (((double)it->second.code - (double)it->second.offset) * 100)
				           / ((double)it->second.gain * divider);
		value.push_back(it->second.value);
	}
};

/**
 *  \brief Устанавливает значение на ЦАП LESO3.
 *  \param[in]	dac	Номер аналогового канала (enum Entity),
 *  				может принимать значения E1 и E2.
 *  \param[in]	value	Значение, выставляемое на ЦАП.
 */
void Leso3Device::setDacValue(Leso3Entity dac, uint16_t value)
{

	char cmd[3] = {0};

	switch (dac)
	{
		case E1:
			cmd[0] = CMD_DAC0;
			break;

		case E2:
			cmd[0] = CMD_DAC1;
			break;

		default:
			throw LesoErrors(ERROR_MSG("Incorrect DAC"), LESO_INVALID_PARAMETER);
	}
	// \todo требуется аппаратная доработка
	if (value >= 4096)
		//throw LesoErrors(ERROR_MSG("Incorrect DAC value"), LESO_INVALID_PARAMETER);
		value = 4095;

	cmd[1] = (char)(value >> 8);
	cmd[2] = (char)(0xFF & value);

#if DEBUG
	cout << __FUNCTION__ <<": " << "dac: " << (int)dac << "; value: " << value << endl;
	cout << hex << (uint16_t)cmd[0] <<' ' << (uint16_t)cmd[1] <<' ' << (uint16_t)cmd[2] << endl;
#endif

	try
	{
		_serial.write(cmd, 3);
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: error"), _serialStatusConvert(e));
	}
};

/**
 *  \brief Устанавливает напряжение на источниках.
 *  \param[in]	e1 Напряжение в Вольтах для истоника E1.
 *  \param[in]	e2 Напряжение в Вольтах для истоника E2.
 */
void Leso3Device::setDacValue(Leso3Entity dac, double value)
{

	LOG_TRACE();
	if ((dac != E1) && (dac != E2))
		throw LesoErrors(ERROR_MSG("Incorrect DAC"), LESO_INVALID_PARAMETER);

	if (analogEntity.at(dac).offset == 0)
		_readCalibrationFactor();

	if (analogEntity.at(dac).offset == 0)
		throw LesoErrors(ERROR_MSG("Error read Coefficients"), LESO_ERROR_CALIBRATION);


	analogEntity.at(dac).value = value;

	analogEntity.at(dac).code = ((double)analogEntity.at(dac).gain/(-1000.0))
								* (double)analogEntity.at(dac).value
				         		+ (double)analogEntity.at(dac).offset;

	setDacValue(dac, analogEntity.at(dac).code);
};


string& Leso3Device::getPortName()
{
	return _currentPortName;
};


void Leso3Device::getCalibrationFactor(
		Leso3Entity ent,
		int32_t &gain,
		int32_t &offset)
{
	if (ent < 0 || ent >= (Leso3Entity)analogEntity.size())
		throw LesoErrors(ERROR_MSG("Incorrect analog entity"), LESO_NOT_SUPPORTED);

	if ( (analogEntity.at(ent).offset == 0)	&& (analogEntity.at(ent).gain == 0) )
		_readCalibrationFactor();

	gain = analogEntity.at(ent).gain;
	offset = analogEntity.at(ent).offset;
};


void Leso3Device::setCalibrationFactor(
		Leso3Entity ent,
		int32_t gain,
		int32_t offset)
{
	if (ent < 0 || ent >= (Leso3Entity)analogEntity.size())
		throw LesoErrors(ERROR_MSG("Incorrect analog entity"), LESO_NOT_SUPPORTED);

	analogEntity.at(ent).gain = gain;
	analogEntity.at(ent).offset = offset;

	_writeCalibrationFactor(ent);

//	analogEntity.at(ent).gain = 0;
//	analogEntity.at(ent).offset = 0;
};

/**
 * \brief Отправляет запрос прибору, если прибор работает, то получаем ответ.
 * \return true  устройство на связи,
 * \return false устройство не отвечает.
 */
bool Leso3Device::test()
{
	LOG_TRACE();
	char cmd[1] = {CMD_NOP};
	char reciveBuff[1] = {0};

	try
	{
		_serial.write(cmd, 1);
		unsigned int bytesReceived;
		bytesReceived = _serial.read(reciveBuff, 2, READ_TIMEOUT_MS);
		if (bytesReceived < 1)
			return false;
		return  (reciveBuff[0] == 0x01);
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: error"), _serialStatusConvert(e));
	}
	catch (...)
	{
		cout << "Unknown error" << endl;
		throw;
	}

	return 0;
}

// private

LesoStatus Leso3Device::_serialStatusConvert(int serialStatus)
{

	switch (serialStatus)
	{
		case Serialib::SERIAL_OK :
			return LESO_OK;

		case Serialib::SERIAL_DEVICE_NOT_FOUND :
		case Serialib::SERIAL_ERROR_OPEN :
			return	LESO_DEVICE_NOT_FOUND;


		case Serialib::SERIAL_ERROR_GET_PARAM :
		case Serialib::SERIAL_ERROR_SET_PARAM :
		case Serialib::SERIAL_ERROR_SPEED :
			return	LESO_INVALID_PARAMETER;

		case Serialib::SERIAL_ERROR_READ :
		case Serialib::SERIAL_ERROR_WRITE :
			return	LESO_IO_ERROR;

		default :
			return	LESO_OTHER_ERROR;
	}
	return LESO_OK;
}

void Leso3Device::_delay_ms(int delay_ms)
{
#if defined (_WIN32) || defined(_WIN64)
	Sleep(delay_ms);
#endif
#ifdef __linux__

#endif
};

void Leso3Device::_setRunMode(Leso3RunMode mode)
{
	if (mode == MODE_RUN)
		_serial.setRTS(false);
	else	// mode == MODE_PROG
		_serial.setRTS(true);
};

/**
 *  Возвраащет true, если в настоящий момент открыт порт;
 *  возвращает false, если не открыт.
 */

bool Leso3Device::isOpened(void)
{
	return _opened;
};

void Leso3Device::_getEeprom(char *value, uint16_t len, uint16_t offset)
{
	char cmd[5] = {0};
	cmd[0] = CMD_EEP_RD;
	cmd[1] = 0xFF & (offset >> 8);
	cmd[2] = 0xFF & offset;
	cmd[3] = 0xFF & (len >> 8);
	cmd[4] = 0xFF & len;

	int bytesReceived = 0;

	try
	{
		_serial.write(cmd, sizeof(cmd));
		bytesReceived = _serial.read(value, len, READ_TIMEOUT_MS);
		if (bytesReceived < len)
			throw LesoErrors(ERROR_MSG("Error len"), LESO_IO_ERROR);
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: error"), _serialStatusConvert(e));
	}
};

void Leso3Device::_setEeprom(char *value, uint16_t len, uint16_t offset)
{
	char cmd[5] = {0};
	cmd[0] = CMD_EEP_WR;
	cmd[1] = 0xFF & (offset >> 8);
	cmd[2] = 0xFF & offset;
	cmd[3] = 0xFF & (len >> 8);
	cmd[4] = 0xFF & len;
    memcpy(&cmd[5], value,  len);
    
	try
	{
		
        _serial.write(cmd, sizeof(cmd) + len);
		//_serial.write(value, len);
		int bytesReceived = _serial.read(cmd, 1, READ_TIMEOUT_MS);
		if ((bytesReceived != 1) || (cmd[0] != 1))
			throw LesoErrors(ERROR_MSG("Error write EEPROM"), LESO_IO_ERROR);
	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: error"), _serialStatusConvert(e));
	}
};


inline uint16_t Leso3Device::_charToInt(char a, char b)
{
	return ((uint16_t)a<<8) | (b & 0xFF);
};

inline uint32_t Leso3Device::_charToInt(char a, char b, char c, char d)
{
	return 	  (((uint32_t)a << 24) & 0xFF000000 )
			| (((uint32_t)b << 16) & 0x00FF0000 )
			| (((uint32_t)c << 8) & 0x0000FF000 )
			| (((uint32_t)d << 0) & 0x0000000FF );
};

inline uint32_t Leso3Device::_charToInt(char *a)
{
	return 	  (((uint32_t)a[0] << 24) & 0xFF000000 )
			| (((uint32_t)a[1] << 16) & 0x00FF0000 )
			| (((uint32_t)a[2] << 8) & 0x00000FF00 )
			| (((uint32_t)a[3] << 0) & 0x0000000FF );
};

/**
 *  Запускает измерения АЦП, полученные значения
 *  записываются во внутренний контейнер
 */
void Leso3Device::_performAdc()
{
	LOG_TRACE();
	char cmd[1] = {CMD_ADC};
	int bytesReceived = 0;

	char readBuff[8];

	try
	{
		int i;
        for ( i = 0; i < 3; i++ )
        {
            _serial.write(cmd, sizeof(cmd));
            bytesReceived = _serial.read(readBuff, sizeof(readBuff), READ_TIMEOUT_MS);
            if (bytesReceived == (int)sizeof(readBuff))
                break;
            else
                cout << "err read adc " << i << endl;
             
        }
        
        if (i == 3)
        {
            throw LesoErrors(ERROR_MSG("Error read ADC"), LESO_IO_ERROR);
        }

	}
	catch (Serialib::SerialPortStatus &e)
	{
		throw LesoErrors(ERROR_MSG("Serial: error"), _serialStatusConvert(e));
	}

//	analogEntity.find(V1)->second.code = _charToInt(readBuff[0],readBuff[1]);
//	analogEntity.find(V2)->second.code = _charToInt(readBuff[2],readBuff[3]);
//	analogEntity.find(A1)->second.code = _charToInt(readBuff[4],readBuff[5]);
//	analogEntity.find(A2)->second.code = _charToInt(readBuff[6],readBuff[7]);

	for (auto it = analogEntity.begin(); it != analogEntity.upper_bound(A2); ++it)
	{
		it->second.code =  _charToInt(readBuff[it->first * 2],readBuff[(it->first * 2) + 1]);
	}


};

/**
 *  Считывает из устройства калибровочные коэффициенты
 */
void Leso3Device::_readCalibrationFactor()
{
	LOG_TRACE();
	char readBuff[48];

	_getEeprom(readBuff, sizeof(readBuff));


#if DEBUG
	cout << "<! Start> " << __FUNCTION__ << endl;
	for (auto i = 0; i < 48; i++)
	{
		if (!(i%4)) cout << endl;
		cout << ((uint16_t)readBuff[i] & 0x00FF) << ' ';

	}
	cout << endl << endl;

	cout  << "ae  offset  gain" << endl;
#endif

	for (auto &ae : analogEntity)
	{
		ae.second.offset = _charToInt(&readBuff[ae.first * 8]);
		ae.second.gain = _charToInt(&readBuff[(ae.first * 8) + 4]);
#if DEBUG
		cout << ae.first << "  " << ae.second.offset << " \t " <<  ae.second.gain << endl;
	}
	cout << endl << "<! End> " << __FUNCTION__ << endl;
#else
	}
#endif

}

/**
 *  Записывает в устройство калибровочные коэффициенты
 */
void Leso3Device::_writeCalibrationFactor(
		Leso3Entity ent
		)
{
	LOG_TRACE();
	char writeBuff[8];

	writeBuff[0] = analogEntity.at(ent).offset >> 24;
	writeBuff[1] = analogEntity.at(ent).offset >> 16;
	writeBuff[2] = analogEntity.at(ent).offset >> 8;
	writeBuff[3] = analogEntity.at(ent).offset;

	writeBuff[4] = analogEntity.at(ent).gain >> 24;
	writeBuff[5] = analogEntity.at(ent).gain >> 16;
	writeBuff[6] = analogEntity.at(ent).gain >> 8;
	writeBuff[7] = analogEntity.at(ent).gain;

#if DEBUG

	for (auto i = 0; i < 8; i++)
	{
		if (!(i%4)) cout << endl;
				cout << ((uint16_t)writeBuff[i] & 0x00FF) << ' ';
	}
	cout << endl;
#endif
	_setEeprom(writeBuff, 8, ent*8);



}







