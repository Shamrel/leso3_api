#include <firmware.h>
#include <iostream>
#include <sstream>
#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <string>

//#include <array>



//#include "listports.h"

#include "leso3.h"

#include "Leso3Device.h"

#include "Leso3Measuring.h"


using namespace std;


#define BUFF_MAX_LEN		(8)

int main(){

//	vector <string> portNameAll;
//	vector <string> portNameLeso3;

	try
	{
		string port = "\\\\.\\COM37";
		vector <string> serialPortAll;
		vector <string> serialPortLeso3;

		Leso3Measuring measure;

		cout << "found leso3: " << measure.searchDevices(serialPortAll, serialPortLeso3) << endl;
		cout << "All ports:" << endl;
		for (auto port : serialPortAll)
			cout << port << endl;
		cout << "leso3: ";
		for (auto port : serialPortLeso3)
			cout << port << endl;

     	//Leso3Index ix = measure.openDevice(R"(\\.\COM37)");
        
        Leso3Index ix = measure.openDevice(serialPortLeso3[0]);

		cout << "ix = " << ix << endl;
		if (ix == LESO3_INVALID_INDEX)
		{
			cout << "Device not found" << endl;
			exit(1);
		}

		cout << "Work with " << measure[ix].getPortName() << endl;

        
        //measure[ix].setCalibrationFactor(E2, 0x88776655, 0xFeFcFdFa);
#if 1

	//	measure[ix].firmwareUpdate(R"(d:\leso\rep\leso3_api\firmware\leso3.hex)");



//		measure[ix].firmwareUpdate(string(R"(../firmware/leso3.hex)"));

		int32_t gain, offset;
		measure[ix].getCalibrationFactor(V2, gain, offset);

		cout << "V2 gain = " << gain << endl;
		cout << "V2 offset = " << offset << endl;

		// gain = -1 * gain;

		//measure[ix].setCalibrationFactor(Leso3Device::V2, gain, offset);

		vector <double> value;
		bool range = 0;
		while (1) {
			double e1;
//			cin >> range;
//			measure[ix].setRange(A2, range);

			// measure[ix].setDacValue(E2, e1 );

			 measure[ix].getAdcValue(value);
// //			cout << "V1" << "\t:\t"
// //				 << "V2" << "\t:\t"
// //				 << "A1" << "\t:\t"
// //				 << "A2" << "\t:\t"
// //				 << endl;
		// //	cout << " -----------------------------------------------------------------" << endl;

			// cout << "V1 = " << value[0] << " (V);" << endl;
			// cout << "V2 = " << value[1] << " (V);" << endl;
			// cout << "A1 = " << value[2] << " (mA);" << endl;
			// cout << "A2 = " << value[3] << " (mA);" << endl;

			// range = !range;

		}
#endif        
	}
	catch (LesoErrors &e)
	{
		cout << "ERROR: " << e.getErrorMessage() << endl;
		cout << "File: "<< e.getErrorFileName() << endl;
		cout << "Function: "<< e.getErrorFunctionName() << endl;
		cout << "Code: " << e.getErrorCode() << endl;
		exit(1);
	}
	catch (...)
	{
		cout << "Unknown error" << endl;
		exit(1);
	}

//	hexfile.printHexFile();










	return 0;
}
